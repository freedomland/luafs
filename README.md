# luaFS

C library to get all custom scripts from custom dir.

Will only works on Linux!

### Why?

You will never need to change any CoreScripts anymore!
 
### How to compile

```bash
gcc luaFS.c -lluajit-5.1 -shared -fPIC -o luaFS.so
```

Put luaFS.so in CoreScripts/lib
 
### How to use

In serverCore.lua after require("time") add require("luaLS") and add following line after that:

```lua
-- scripts dir keep changing every fucking time, so we need to get it from server
local scripts_dir = string.split(tes3mp.GetModDir(), "/")[2]
local custom_scripts = string.format("./%s/scripts/custom", scripts_dir)

for ind, m in pairs(listFiles(custom_scripts)) do
	print("Loading custom module " .. m)
	require("custom/" .. m)
end
```

### License
luaFS.so (c) 2019 Freedom Land Team, GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
