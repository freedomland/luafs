/*
 * betterLuaRandom.so (c) 2019 Freedom Land Team, GPL v3.0
 * 
 *  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * */

#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "luajit-2.0/lua.h"
#include "luajit-2.0/lualib.h"
#include "luajit-2.0/lauxlib.h"

static int listFile(lua_State *L) 
{
	const char *mod_dir = luaL_checkstring(L, 1);
	
	DIR *d = opendir(mod_dir);
	struct dirent *dir;
		
	int i = 0;
	
	if (d) 
	{
		lua_newtable(L);
		
		while ((dir = readdir(d)) != NULL) 
		{
			// ignore . and ..
			if (strncmp(dir->d_name, ".", 1) == 0 || strncmp(dir->d_name, "..", 2) == 0)
				continue;
			
			// Only list .lua extention
			if (strstr(dir->d_name, ".lua"))
			{
				// DON'T free this token, strtok manipulating strings itself, no memory allocated
				// No, strsep is not option, and strtok_r is overkill
				char *token = strtok(dir->d_name, ".");
				
				lua_pushstring(L, token);
				lua_rawseti(L, -2, i+1);
				i++;
			}
		}
		
		closedir(d);
	}
	
	return 1;
}


int luaopen_luaLS(lua_State *L)
{
	lua_register(L, "listFiles", listFile);
	return 0;
}

